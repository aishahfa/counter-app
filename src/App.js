import React, { Component } from "react";
import NavBar from "./components/navbar.jsx";
import Counters from "./components/counters.jsx";
import "./App.css";

class App extends Component {
  state = {
    counters: [
      { id: 1, value: 4 },
      { id: 2, value: 0 },
      { id: 3, value: 1 },
      { id: 4, value: 0 },
    ],
  };

  //constructor called only once when an istance of a class is created
  constructor(props) {
    super(props);
    console.log("App - Constructor");
    //this.state = this.props.something; //if need to set the state directly, do so in constructor
  }

  //this mean the components is in the DOM
  componentDidMount() {
    console.log("App - Mounted");
  }

  handleIncrement = (counter) => {
    //not update the state directly
    //create new counters array and give it to the setstate & have react update the state
    const counters = [...this.state.counters]; //cloning the array into counters
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value++;
    this.setState({ counters });
  };

  handleReset = () => {
    const counters = this.state.counters.map((c) => {
      c.value = 0;
      return c;
    });
    this.setState({ counters });
  };

  handleDelete = (counterId) => {
    //console.log("Event handler: handleDelete called", counterId);
    const counters = this.state.counters.filter((c) => c.id !== counterId);
    //replacing the current state with counters id that is not deleted
    this.setState({ counters });
  };

  //returns a react element represents the virtual DOM then render it in actual browser DOM
  render() {
    console.log("App - Rendered");

    return (
      <React.Fragment>
        <NavBar
          totalCounters={this.state.counters.filter((c) => c.value > 0).length}
        />
        <main className="container">
          <Counters
            counters={this.state.counters}
            onReset={this.handleReset}
            onIncrement={this.handleIncrement}
            onDelete={this.handleDelete}
          />
        </main>
      </React.Fragment>
    );
  }
}

export default App;
